/*
 * Copyright © 2021-2022 UBports Foundation.
 *
 * Contact: Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOMIRI_MEDIAHUBSERVICE_DBUS_CLIENT_DEATH_OBSERVER_H
#define LOMIRI_MEDIAHUBSERVICE_DBUS_CLIENT_DEATH_OBSERVER_H

#include "client_death_observer_p.h"

#include <QDBusServiceWatcher>
#include <QObject>
#include <QVector>

namespace lomiri {
namespace MediaHubService {

class DBusClientDeathObserver: public QObject,
                               public ClientDeathObserverPrivate
{
    Q_OBJECT

public:
    DBusClientDeathObserver(ClientDeathObserver *q);
    ~DBusClientDeathObserver();

    // Registers the given client for death notifications.
    void registerForDeathNotifications(const Player::Client &) override;

protected:
    void onServiceDied(const QString &serviceName);

private:
    QVector<Player::Client> m_clients;
    QDBusServiceWatcher m_watcher;
};

}
}

#endif // LOMIRI_MEDIAHUBSERVICE_DBUS_CLIENT_DEATH_OBSERVER_H
