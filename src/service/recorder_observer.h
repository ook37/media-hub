/*
 * Copyright © 2014 Canonical Ltd.
 * Copyright © 2022 UBports Foundation.
 *
 * Contact: Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Thomas Voß <thomas.voss@canonical.com>
 */

#ifndef LOMIRI_MEDIAHUBSERVICE_RECORDER_OBSERVER_H
#define LOMIRI_MEDIAHUBSERVICE_RECORDER_OBSERVER_H

#include <QObject>
#include <QScopedPointer>

namespace lomiri
{
namespace MediaHubService
{
// All known states of the recorder
enum class RecordingState
{
    // No active recording
    stopped,
    // We have an active recording session
    started
};

// A RecorderObserver allows for monitoring the recording state
// of the service.
class RecorderObserverPrivate;
class RecorderObserver: public QObject
{
    Q_OBJECT

public:
    RecorderObserver(QObject *parent = nullptr);
    RecorderObserver(const RecorderObserver&) = delete;
    virtual ~RecorderObserver();
    RecorderObserver& operator=(const RecorderObserver&) = delete;

    RecordingState recordingState() const;

Q_SIGNALS:
    void recordingStateChanged();

private:
    Q_DECLARE_PRIVATE(RecorderObserver);
    QScopedPointer<RecorderObserverPrivate> d_ptr;
};

}
}

#endif // LOMIRI_MEDIAHUBSERVICE_RECORDER_OBSERVER_H
